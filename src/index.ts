import express from 'express';
import socketIO from 'socket.io';
import cors from 'cors';

import router from './router';
import {addUser, getUser, removeUser, userProps} from './users';

const PORT = process.env.PORT || 5000;

const app = express();
const server = app.listen(PORT);
const io = socketIO().listen(server);

// options for cors middleware
const options:cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "http://localhost:3000",
    preflightContinue: false
};


io.on("connect", (socket) => {
    let user:userProps;
    socket.on('join', ({id, name}) => {
        try {
            user = addUser({id, name});
            console.log(`User (${id}, ${name}) connected`);
            socket.emit('message', {text: `Successfully connected`});
        }
        catch (e) {
            console.log(e);
        }
    });

    socket.on('move', ({angle, force}) => {
        if(user && angle && force)
        {
            console.log(`${user.name} angle event: ${angle.degree} | force event: ${force}`);
        }
    });

    socket.on('paint', ({matrix}) => {
        if(user && matrix)
        {
            console.log(`${user.name} matrix event: ${matrix}`);
        }
    });

    socket.on('disconnect', () => {
        if(user)
        {
            removeUser(user.id);
            console.log(`User (${user.id}, ${user.name}) disconnected`);
        }
    });
});



app.use(cors(options));
app.use(router);